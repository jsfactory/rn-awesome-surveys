import React, { useState, useRef } from 'react'
import { View, Text, ScrollView, SafeAreaView } from 'react-native'
import {
  TextInput,
  FAB,
  List,
  Button,
  Headline,
  Subheading,
  Modal,
  Portal,
  Snackbar,
} from 'react-native-paper'
import { createSurvey } from '@/Store/Surveys'
import { useSelector, useDispatch } from 'react-redux'
import Wizard from 'react-native-wizard'
import ToggleButton from 'react-native-toggle-buttons'

export default function NewSurvey({ navigation }) {
  const [title, settitle] = useState('')
  const [reward, setreward] = useState(10)
  const [questions, setquestions] = useState([])
  const [questionModalVisible, setquestionModalVisible] = useState(false)
  const [questionType, setquestionType] = useState('TextInput')
  const [questionText, setquestionText] = useState('')
  const [saving, setsaving] = useState(false)
  const [errorMessage, seterrorMessage] = useState('')

  const [_, setCurrentStep] = useState()
  const [isFirstStep, setIsFirstStep] = useState()
  const [isLastStep, setIsLastStep] = useState()

  const wizard = useRef(null)

  const dispatch = useDispatch()

  const onDismissSnackBar = () => seterrorMessage(null)

  const addQuestion = () => {
    setquestions([...questions, { questionText, questionType }])
    setquestionModalVisible(false)
    setquestionType('TextInput')
    setquestionText('')
  }

  const save = async () => {
    setsaving(true)
    try {
      await dispatch(
        createSurvey({
          title,
          reward,
          questions,
        }),
      )
      navigation.navigate('Profile')
    } catch (error) {
      seterrorMessage('something went wrong please try again in a few moments')
    }
    setsaving(false)
  }

  const stepList = [
    {
      content: (
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <TextInput
            mode="outlined"
            label="Title"
            value={title}
            onChangeText={settitle}
          />
        </View>
      ),
    },
    {
      content: (
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <TextInput
            mode="outlined"
            label="Reward"
            value={`${reward}`}
            onChangeText={setreward}
          />
        </View>
      ),
    },
    {
      content: (
        <View style={{ flex: 1 }}>
          <ScrollView>
            <List.AccordionGroup>
              {questions.map(({ questionText, questionType }, index) => (
                // <List.Accordion title={`question ${index + 1}`} id={index + 1}>
                //   <TextInput mode={'outlined'} label={'question'} />
                // </List.Accordion>
                <List.Item
                  key={index}
                  title={questionText}
                  description={questionType}
                  style={{ backgroundColor: 'white' }}
                  // left={props => <List.Icon {...props} icon="help" />}
                />
              ))}
            </List.AccordionGroup>
            <Button
              style={{ marginVertical: 5 }}
              mode={'outlined'}
              icon={'plus'}
              compact
              onPress={() => setquestionModalVisible(true)}
            >
              add a question
            </Button>
          </ScrollView>
        </View>
      ),
    },
  ]

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={{ flex: 1, padding: 10 }}>
        <Portal>
          <Modal
            visible={questionModalVisible}
            onDismiss={() => setquestionModalVisible(false)}
            contentContainerStyle={{
              backgroundColor: 'white',
              padding: 5,
              margin: 5,
            }}
          >
            <Subheading style={{ marginVertical: 5, marginHorizontal: 5 }}>
              Question Type
            </Subheading>
            <ToggleButton.Group
              // row={false}
              selected={questionType}
              onValueChange={setquestionType}
            >
              {/* <ToggleButton title={'Info'} value={'Info'} /> */}
              <ToggleButton title={'Text'} value={'TextInput'} />
              <ToggleButton title={'Numeric'} value={'NumericInput'} />
              <ToggleButton title={'Selection'} value={'SelectionGroup'} />
              <ToggleButton
                title={'Multiselection'}
                value={'MultipleSelectionGroup'}
              />
            </ToggleButton.Group>
            <TextInput
              mode={'outlined'}
              label={'question text'}
              value={questionText}
              onChangeText={setquestionText}
            />
            {(questionType === 'SelectionGroup' ||
              questionType === 'MultipleSelectionGroup') && (
              <View style={{ marginVertical: 5 }}>
                <Button icon={'plus'} mode={'outlined'}>
                  add option
                </Button>
              </View>
            )}

            <Button
              style={{ marginVertical: 5 }}
              icon={'plus'}
              mode={'contained'}
              onPress={addQuestion}
            >
              add question
            </Button>
          </Modal>
        </Portal>
        <View style={{ flex: 1 }}>
          <Wizard
            ref={wizard}
            contentContainerStyle={{ flex: 1 }}
            activeStep={0}
            steps={stepList}
            isFirstStep={val => setIsFirstStep(val)}
            isLastStep={val => setIsLastStep(val)}
            // onNext={() => {
            //   console.log('Next Step Called')
            // }}
            // onPrev={() => {
            //   console.log('Previous Step Called')
            // }}
            currentStep={({ currentStep, isLastStep, isFirstStep }) => {
              setCurrentStep(currentStep)
            }}
          />
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <Button
            mode="contained"
            disabled={isFirstStep}
            onPress={() => wizard.current.prev()}
          >
            Prev
          </Button>
          {isLastStep ? (
            <Button
              mode="contained"
              onPress={save}
              disabled={questions.length === 0}
              loading={saving}
            >
              save
            </Button>
          ) : (
            <Button
              mode="contained"
              disabled={isLastStep}
              onPress={() => wizard.current.next()}
            >
              Next
            </Button>
          )}
        </View>
        {/* <FAB
        style={{
          position: 'absolute',
          margin: 15,
          bottom: 0,
          alignSelf: 'center',
        }}
        icon={'check'}
        label={'save'}
        onPress={save}
      /> */}
        {errorMessage ? (
          <Snackbar visible onDismiss={onDismissSnackBar}>
            {errorMessage}
          </Snackbar>
        ) : null}
      </View>
    </SafeAreaView>
  )
}
