import React, { useState, useEffect } from 'react'
import { View, Text, Linking } from 'react-native'
import {
  TextInput,
  Button,
  Headline,
  Subheading,
  Card,
  Caption,
  Checkbox,
} from 'react-native-paper'
import { useSelector, useDispatch } from 'react-redux'
import { signIn, signUp } from '@/Store/Session'
import { Config } from '@/Config'

export default function Auth() {
  const [email, setemail] = useState('')
  const [password, setpassword] = useState('')
  // const [email, setemail] = useState('solomonk6161@gmail.com')
  // const [password, setpassword] = useState('SOLOmonk61')
  const [username, setusername] = useState('')
  const [loading, setloading] = useState(false)
  const [isLogin, setisLogin] = useState(true)

  const [checked, setChecked] = useState(false)

  const session = useSelector(state => state.session)
  const dispatch = useDispatch()

  const signin = async () => {
    setloading(true)
    await dispatch(signIn({ identifier: email, password }))
    setloading(false)
  }
  const signup = async () => {
    setloading(true)
    await dispatch(signUp({ username, email, password }))
    setloading(false)
  }

  const _main_button = (
    <Button
      icon="login"
      mode="contained"
      onPress={isLogin ? signin : signup}
      loading={loading}
      disabled={!checked}
    >
      {isLogin ? 'login' : 'register'}
    </Button>
  )
  const _secondary_button = (
    <Button mode={'outlined'} onPress={() => setisLogin(!isLogin)} compact>
      {isLogin ? 'create account' : 'already registred?'}
    </Button>
  )

  return (
    <View
      style={{ flex: 1, justifyContent: 'center', backgroundColor: 'white' }}
    >
      <View style={{ padding: 10 }}>
        {!isLogin && (
          <TextInput
            label="Name"
            mode="outlined"
            value={username}
            onChangeText={setusername}
          />
        )}
        <TextInput
          label="Email"
          mode="outlined"
          value={email}
          onChangeText={setemail}
        />
        <TextInput
          secureTextEntry
          label="Password"
          mode="outlined"
          value={password}
          onChangeText={setpassword}
        />
        <View style={{ flexDirection: 'row', marginVertical: 5 }}>
          <Checkbox
            status={checked ? 'checked' : 'indeterminate'}
            onPress={() => {
              setChecked(!checked)
            }}
          />
          <Caption>
            Yes, i understand and agree to the
            <Text
              onPress={() => Linking.openURL(Config.TERMS_URL)}
              style={{ color: 'purple' }}
            >
              {' '}
              Terms of service
            </Text>{' '}
            , including the user
            <Text
              onPress={() => Linking.openURL(Config.PRIVACY_POLICY_URL)}
              style={{ color: 'purple' }}
            >
              {' '}
              privacy policy
            </Text>
          </Caption>
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'baseline',
            justifyContent: 'space-between',
            marginTop: 10,
          }}
        >
          {_main_button}
          {/* <Subheading style={{ paddingHorizontal: 20 }}>or</Subheading> */}
          {_secondary_button}
        </View>
        {session.notFound && (
          <Card
            style={{ marginVertical: 5, borderColor: 'red', borderWidth: 1 }}
          >
            <Card.Title title={'Identifier or password invalid'} />
          </Card>
        )}
      </View>
    </View>
  )
}
