import { View, Text, ScrollView } from 'react-native'
import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Button, TextInput, List, Chip } from 'react-native-paper'
import { signOut, refresh } from '@/Store/Session'
import { updateSurvey } from '@/Store/Surveys'

export default function Profile({ navigation }) {
  const session = useSelector(state => state.session)
  const dispatch = useDispatch()

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      dispatch(refresh())
    })

    return unsubscribe
  })

  const update = async data => {
    await dispatch(updateSurvey(data))
    dispatch(refresh())
    console.log('updated')
  }

  if (!session.user?.surveys) {
    return (
      <View style={{ flex: 1, justifyContent: 1 }}>
        <Text>please login</Text>
      </View>
    )
  }

  return (
    <View style={{ flex: 1 }}>
      {/* <Text>{JSON.stringify(session.user.surveys)}</Text> */}
      <ScrollView contentContainerStyle={{ flex: 1 }}>
        <List.Section>
          <List.Subheader>My Surveys</List.Subheader>
          {session.user.surveys.map((survey, index) => {
            return (
              <List.Item
                key={index}
                title={`${survey.title} ${
                  survey.status === 'pending' ? '(pending)' : ''
                }`}
                description={`${survey.questions.length || 0} questions`}
                left={props => <List.Icon {...props} icon="list-status" />}
                right={props => (
                  <View style={{ alignItems: 'center', flexDirection: 'row' }}>
                    {/* <Button compact mode={'outlined'} icon={'wallet-plus'}>
                      {survey.reward}
                    </Button> */}
                    {survey.status === 'approved' ? (
                      <Button
                        compact
                        mode={'outlined'}
                        icon={'pause'}
                        style={{ margin: 2 }}
                        color={'red'}
                        onPress={() =>
                          update({ id: survey.id, status: 'paused' })
                        }
                      >
                        pause
                      </Button>
                    ) : survey.status === 'paused' ? (
                      <Button
                        compact
                        mode={'outlined'}
                        icon={'play'}
                        style={{ margin: 2 }}
                        color={'green'}
                        onPress={() =>
                          update({ id: survey.id, status: 'approved' })
                        }
                      >
                        resume
                      </Button>
                    ) : null}
                  </View>
                )}
                style={{ backgroundColor: 'white' }}
                onPress={() => navigation.navigate('Answers', { survey })}
              />
            )
          })}
        </List.Section>
      </ScrollView>

      <Button
        style={{ borderRadius: 0 }}
        mode="contained"
        onPress={() => dispatch(signOut())}
      >
        Signout
      </Button>
    </View>
  )
}
