import React from 'react'
import { View, Text, Image, useWindowDimensions } from 'react-native'
import AppIntroSlider from 'react-native-app-intro-slider'
import { Headline, Paragraph, Subheading, Button } from 'react-native-paper'
import { useDispatch } from 'react-redux'
import { skipIntro } from '@/Store/LocalConfig'

const slides = [
  {
    key: 'one',
    title: 'Take Survey',
    text: 'Take Surveys and earn points',
    image: require('@/Assets/Images/1.jpg'),
    backgroundColor: '#ededed',
  },
  {
    key: 'two',
    title: 'Publish Survey',
    text: 'You can publish your own surveys and get answers from our community',
    image: require('@/Assets/Images/2.jpg'),
    backgroundColor: '#fff',
  },
  {
    key: 'three',
    title: 'Reward System',
    text: 'Get rewards answering community surveys and spend them on your own surveys',
    image: require('@/Assets/Images/3.jpg'),
    backgroundColor: '#eaecf9',
  },
]

export default function Intro() {
  const dispatch = useDispatch()
  const { width } = useWindowDimensions()

  const _renderItem = ({ item }) => {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: item.backgroundColor,
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Headline style={{ padding: 20 }}>{item.title}</Headline>
        <Image
          style={{ height: width, width }}
          resizeMode={'contain'}
          source={item.image}
        />
        <Subheading style={{ fontSize: 18, padding: 20 }}>
          {item.text}
        </Subheading>
      </View>
    )
  }

  const _renderNextButton = () => {
    return (
      <Button mode={'outlined'} color={'black'}>
        Next
      </Button>
    )
  }
  const _renderDoneButton = () => {
    return (
      <Button mode={'outlined'} color={'black'}>
        Done
      </Button>
    )
  }

  const onDone = () => {
    dispatch(skipIntro())
  }

  return (
    <AppIntroSlider
      dotStyle={{ backgroundColor: '#aaa' }}
      activeDotStyle={{ backgroundColor: '#333' }}
      renderItem={_renderItem}
      renderNextButton={_renderNextButton}
      renderDoneButton={_renderDoneButton}
      data={slides}
      onDone={onDone}
    />
  )
}
