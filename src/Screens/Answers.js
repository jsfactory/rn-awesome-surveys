import React, { useEffect, useState } from 'react'
import { View, Text, ScrollView, SafeAreaView } from 'react-native'
import {
  ActivityIndicator,
  Card,
  Subheading,
  Title,
  Paragraph,
  Chip,
  FAB,
  Caption,
} from 'react-native-paper'
import axios from 'axios'
import { Config } from '@/Config'
// import { useSelector } from 'react-redux'
import moment from 'moment'

export default function Answers({ route, navigation }) {
  const { survey } = route.params
  // const session = useSelector(state => state.session)
  // const dispatch = useDispatch()
  const [data, setdata] = useState()
  const [loading, setloading] = useState()

  const fetch = async () => {
    const response = await axios.get(`${Config.API_URL}/surveys/${survey.id}`)
    setdata(response.data)
  }

  useEffect(() => {
    fetch()
  })

  if (!data) {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator />
      </View>
    )
  }

  if (data.answers.length === 0) {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Subheading> this survey doesn't have any answers yet</Subheading>
      </View>
    )
  }

  // return <Text>{JSON.stringify(data.answers)}</Text>

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={{ flex: 1 }}>
        <ScrollView>
          {data.answers.map((answer, index) => (
            <Card key={index}>
              <Card.Content>
                {/* <Title>{moment(answer.created_at).fromNow()}</Title> */}
                <Paragraph>
                  <View>
                    {Object.values(answer.content).map((a, i) => (
                      // <Chip key={i}>{a}</Chip>
                      // <Chip key={i}>{JSON.stringify(a)}</Chip>
                      <Caption key={i}>
                        {i + 1}:{a['optionText'] || JSON.stringify(a)}
                      </Caption>
                    ))}
                    {/* <Text>{JSON.stringify(answer.content)}</Text> */}
                  </View>
                </Paragraph>
                <Subheading>{moment(answer.created_at).fromNow()}</Subheading>
              </Card.Content>
            </Card>
          ))}
        </ScrollView>
        {/* <FAB
          loading={loading}
          label="Download Answers CSV"
          style={{ alignSelf: 'center', position: 'absolute', bottom: 0 }}
        ></FAB> */}
      </View>
    </SafeAreaView>
  )
}
