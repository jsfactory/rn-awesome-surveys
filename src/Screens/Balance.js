import { View, Text, SafeAreaView, Share } from 'react-native'
import React from 'react'
import { useSelector } from 'react-redux'
import { Headline, Subheading, Button } from 'react-native-paper'

export default function Balance({ navigation }) {
  const session = useSelector(state => state.session)

  return (
    <View style={{ flex: 1, padding: 20 }}>
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Headline>Survey Balance</Headline>
        <Button compact labelStyle={{ fontSize: 50 }} icon={'wallet'}>
          {session.user.balance}
        </Button>
      </View>
      <View>
        <Button
          labelStyle={{ fontSize: 18 }}
          mode={'contained'}
          onPress={() =>
            Share.share({
              message: `i earned ${session.user.balance} points on Awesome Surveys answering surveys`,
            })
          }
        >
          Share Progress
        </Button>
        <View style={{ height: 10 }} />
        <Button
          labelStyle={{ fontSize: 18 }}
          mode={'outlined'}
          onPress={() => navigation.navigate('Main')}
        >
          more surveys
        </Button>
      </View>
    </View>
  )
}
