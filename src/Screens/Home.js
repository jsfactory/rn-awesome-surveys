import React, { useEffect } from 'react'
import { View, Text, FlatList } from 'react-native'
import { Card, SimpleCard } from '@paraboly/react-native-card'
import { useSelector, useDispatch } from 'react-redux'
import { fetchLatestSurveys } from '@/Store/Surveys'
import { refresh } from '@/Store/Session'
import { ActivityIndicator, FAB } from 'react-native-paper'

export default function Home({ navigation }) {
  const surveys = useSelector(state => state.surveys)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(refresh())
    const promise = dispatch(fetchLatestSurveys())
    return () => {
      promise.abort()
    }
  }, [])

  const renderItem = ({ item }) => (
    <View
      key={item.id}
      style={{ flex: 1, paddingTop: 8, alignItems: 'center' }}
    >
      <Card
        iconName="question"
        iconType={'FontAwesome'}
        title={item.title}
        description={item.description}
        bottomRightText={item.reward}
        topRightText={`${item.questions?.length || 0} questions`}
        onPress={() => navigation.navigate('Survey', { item })}
      />
    </View>
  )

  if (surveys.fetching) {
    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <ActivityIndicator size={'large'} />
      </View>
    )
  }
  return (
    <View style={{ flex: 1 }}>
      {/* <Text>{JSON.stringify(surveys.data)}</Text> */}
      <FlatList
        // data={surveys.data.map(item => ({
        //   ...item,
        //   questions: JSON.parse(item.questions),
        // }))}
        data={surveys.data}
        renderItem={renderItem}
        keyExtractor={item => item.id}
      />
      <FAB
        style={{
          position: 'absolute',
          margin: 15,
          bottom: 0,
          alignSelf: 'center',
        }}
        icon="plus"
        label="post survey"
        onPress={() => navigation.navigate('NewSurvey')}
      />
    </View>
  )
}
