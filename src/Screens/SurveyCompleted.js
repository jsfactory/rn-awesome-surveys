import React, { useEffect } from 'react'
import { Text, View, Image } from 'react-native'
import { Button, Headline, FAB, Subheading } from 'react-native-paper'
import { useDispatch } from 'react-redux'
import { answerSurvey } from '@/Store/Surveys'

export default function SurveyCompleted({ route, navigation }) {
  const { survey, answers: content } = route.params
  const dispatch = useDispatch()

  const saveAnswer = async () => {
    await dispatch(
      answerSurvey({
        survey: survey.id,
        content,
        reward: survey.reward,
      }),
    )
    navigation.navigate('Profile')
  }

  return (
    <View style={{ flex: 1, padding: 25, backgroundColor: 'white' }}>
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Headline>
          {/* You succesfully answered {survey.questions.length} questions of the
          survey */}
          Thank you
        </Headline>
        <Subheading>
          for answering {survey.questions.length} questions
        </Subheading>
        <Image
          source={require('@/Assets/Images/trophy-cup.jpg')}
          style={{ width: 200, height: 200, marginVertical: 10 }}
          resizeMode={'contain'}
        />
        <FAB
          onPress={saveAnswer}
          style={{ marginVertical: 20 }}
          label={`Save and earn ${survey.reward} points`}
        ></FAB>
      </View>
    </View>
  )
}
