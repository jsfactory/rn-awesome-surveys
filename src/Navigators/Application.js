import React from 'react'
import { SafeAreaView, StatusBar } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'
import { NavigationContainer } from '@react-navigation/native'
import { useSelector } from 'react-redux'
// import { StartupContainer } from '@/Containers'
import { useTheme } from '@/Hooks'
import {
  Survey,
  SurveyCompleted,
  Auth,
  NewSurvey,
  Intro,
  Answers,
  Balance,
} from '@/Screens'
import { BalanceButton } from '@/Components'
import MainNavigator from './Main'
import { navigationRef } from './utils'
import { Config } from '@/Config'

const Stack = createStackNavigator()

// @refresh reset
const ApplicationNavigator = () => {
  const { Layout, darkMode, NavigationTheme } = useTheme()
  const { colors } = NavigationTheme

  const session = useSelector(state => state.session)
  const local = useSelector(state => state.localConfig)

  return (
    // <SafeAreaView style={[Layout.fill, { backgroundColor: colors.card }]}>
    <NavigationContainer theme={NavigationTheme} ref={navigationRef}>
      <StatusBar barStyle={darkMode ? 'light-content' : 'dark-content'} />
      <Stack.Navigator>
        {!local.introSeen && (
          <Stack.Screen
            name="Startup"
            component={Intro}
            options={{ headerShown: false }}
          />
        )}
        {session?.user ? (
          <>
            <Stack.Screen
              name="Main"
              component={MainNavigator}
              options={({ navigation }) => ({
                animationEnabled: false,
                title: Config.APP_NAME,
                headerRight: () => (
                  <BalanceButton
                    balance={session.user.balance}
                    onPress={() => navigation.navigate('Balance')}
                  />
                ),
              })}
            />
            <Stack.Screen
              name="Survey"
              component={Survey}
              options={({ route }) => ({
                title: route.params?.item?.title || 'survey not found',
                headerTransparent: true,
                headerBackTitle: '',
              })}
            />
            <Stack.Screen
              name="SurveyCompleted"
              component={SurveyCompleted}
              options={{
                title: '',
                headerTransparent: true,
              }}
            />
            <Stack.Screen
              name="NewSurvey"
              component={NewSurvey}
              options={{
                title: '',
                headerTransparent: true,
                headerBackTitle: '',
              }}
            />
            <Stack.Screen name="Answers" component={Answers} />
            <Stack.Screen
              name="Balance"
              component={Balance}
              options={{ headerShown: false }}
            />
          </>
        ) : (
          <Stack.Screen
            name="Auth"
            component={Auth}
            options={{ headerShown: false }}
          />
        )}
      </Stack.Navigator>
    </NavigationContainer>
    // </SafeAreaView>
  )
}

export default ApplicationNavigator
