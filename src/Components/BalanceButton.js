import React from 'react'
import { View, Text } from 'react-native'
import { Button } from 'react-native-paper'

export default function BalanceButton({ balance = 0, onPress }) {
  return (
    <Button
      compact
      icon={'wallet'}
      color={'gold'}
      mode={'contained'}
      style={{ marginRight: 5 }}
      onPress={onPress}
    >
      {balance}
    </Button>
  )
}
