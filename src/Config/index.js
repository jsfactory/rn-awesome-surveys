export const Config = {
  APP_NAME: 'Awesome Surveys',
  API_URL: 'https://survey.21web.store',
  PRIVACY_POLICY_URL:
    'https://sites.google.com/view/place1mobile/privacy-policy?authuser=0',
  TERMS_URL:
    'https://sites.google.com/view/place1mobile/terms-conditions?authuser=0',
}
