import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'
import { Config } from '@/Config'

export const signIn = createAsyncThunk(
  'auth/signin',
  async ({ identifier, password }) => {
    const response = await axios.post(`${Config.API_URL}/auth/local`, {
      identifier,
      password,
    })
    return response.data
  },
)

export const signUp = createAsyncThunk(
  'auth/signup',
  async ({ username, email, password }) => {
    const response = await axios.post(`${Config.API_URL}/auth/local/register`, {
      username,
      email,
      password,
      balance: 10,
    })
    return response.data
  },
)

export const refresh = createAsyncThunk(
  'auth/refresh',
  async (_, { getState }) => {
    const { session } = getState()
    const response = await axios.get(
      `${Config.API_URL}/users/${session.user.id}`,
      {
        headers: {
          Authorization: `Bearer ${session.jwt}`,
        },
      },
    )
    return response.data
  },
)

const initialState = {}

const slice = createSlice({
  name: 'surveys',
  initialState,
  reducers: {
    signOut: state => {
      state.user = null
      state.jwt = null
    },
  },
  extraReducers: builder => {
    builder
      .addCase(signIn.fulfilled, (state, { payload }) => {
        state.notFound = false
        state.jwt = payload.jwt
        state.user = payload.user
      })
      .addCase(signUp.fulfilled, (state, { payload }) => {
        state.notFound = false
        state.jwt = payload.jwt
        state.user = payload.user
      })
      .addCase(refresh.fulfilled, (state, { payload }) => {
        state.user = payload
      })
      .addCase(signIn.rejected, state => {
        state.notFound = true
      })
      .addCase(signUp.rejected, state => {
        state.notFound = true
      })
  },
})

export const { signOut } = slice.actions

export default slice.reducer
