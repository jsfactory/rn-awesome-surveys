import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'
import { Config } from '@/Config'

export const fetchLatestSurveys = createAsyncThunk(
  'surveys/fetch',
  async () => {
    const response = await axios.get(
      `${Config.API_URL}/surveys?status=approved`,
    )
    return response.data
  },
)

export const fetchSurvey = createAsyncThunk('surveys/details', async id => {
  const response = await axios.get(`${Config.API_URL}/survey/${id}`)
  return response.data
})

export const createSurvey = createAsyncThunk(
  'surveys/new',
  async (data, { getState }) => {
    const { session } = getState()
    const response = await axios.post(
      `${Config.API_URL}/surveys`,
      { ...data, status: 'pending', users_permissions_user: session.user.id },
      {
        headers: {
          Authorization: `Bearer ${session.jwt}`,
        },
      },
    )

    return response.data
  },
)

export const updateSurvey = createAsyncThunk(
  'surveys/update',
  async ({ id, status }, { getState }) => {
    const { session } = getState()
    const response = await axios.put(
      `${Config.API_URL}/surveys/${id}`,
      { status },
      {
        headers: {
          Authorization: `Bearer ${session.jwt}`,
        },
      },
    )

    return response.data
  },
)

export const answerSurvey = createAsyncThunk(
  'surveys/answer',
  async ({ reward, user, ...data }, { getState }) => {
    const { session } = getState()
    const answerReq = await axios.post(
      `${Config.API_URL}/answers`,
      { ...data, users_permissions_user: session.user.id },
      {
        headers: {
          Authorization: `Bearer ${session.jwt}`,
        },
      },
    )

    // const incrementReq = await axios.put(
    //   `${Config.API_URL}/users/${user}`,
    //   { balance: 10 },
    //   {
    //     headers: {
    //       Authorization: `Bearer ${session.jwt}`,
    //     },
    //   },
    // )

    const decrementReq = await axios.put(
      `${Config.API_URL}/users/${session.user.id}`,
      { balance: session.user.balance + reward },
      {
        headers: {
          Authorization: `Bearer ${session.jwt}`,
        },
      },
    )

    const responses = await axios.all([
      answerReq /*incrementReq,*/,
      decrementReq,
    ])
    return responses[0].data
  },
)

const initialState = { fetching: true, data: [] }

const slice = createSlice({
  name: 'surveys',
  initialState,
  reducers: {
    requestData: state => {
      state = initialState
    },
  },
  extraReducers: builder => {
    builder.addCase(fetchLatestSurveys.fulfilled, (state, { payload }) => {
      if (Array.isArray(payload)) {
        state.fetching = false
        state.data = payload
      }
    })
  },
})

export const { requestData } = slice.actions

export default slice.reducer
