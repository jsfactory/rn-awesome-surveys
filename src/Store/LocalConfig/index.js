import { createSlice } from '@reduxjs/toolkit'

const initialState = { introSeen: false }

const slice = createSlice({
  name: 'localConfig',
  initialState,
  reducers: {
    skipIntro: state => {
      state.introSeen = true
    },
  },
})

export const { skipIntro } = slice.actions

export default slice.reducer
